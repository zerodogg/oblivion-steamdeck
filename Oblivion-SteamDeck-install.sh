#!/bin/bash
# Oblivion SteamDeck mod installer
# Copyright (C) Eskild Hustvedt 2023
#
# Installs components from xOBSE (https://github.com/llde/xOBSE) and
# NorthernUI Away (https://www.nexusmods.com/oblivion/mods/48577). Those
# components have their own licenses.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERSION="0.1"
NORTHERNUI_FILE="https://gitlab.com/zerodogg/oblivion-steamdeck/-/raw/06a74e81722f7a3f1a1e46a7efe6705248817c4e/3rdparty/NorthernUIAway-Vanilla_Style-48577-2-0-3-1-1621709511.zip?inline=false"
NORTHERNUI_SHA512SUM="2ff457fa1c72a843b9e3ea10854f512a67ef990ee35d2dff9a2311e560c18920dae32de9870973e90b49c2fd730584c04a70472caa16fd11b625831615522112"

OBSE_FILE="https://github.com/llde/xOBSE/releases/download/22.9/xOBSE-22.9.zip"
OBSE_SHA512SUM="084a0cec4196933aa1c8df0c59b70ec7bb253c2fb9903337b6dd5d9e7d7ad5bef35926f1724d07d443046228908c3664b6ca839aaea6393c747774f53491014d"

OBLIVION_PATH=""

function verifyDeps ()
{
    ret=0
    for tool in wget perl unzip grep mv cp; do
        if ! type $tool &>/dev/null; then
            echo 1>&2 "Dependency is missing: $tool"
            ret=1
        fi
    done
    if [ "$ret" != "0" ]; then
        cleanExit $ret
    fi
}

function cleanExit ()
{
    if [ -e "$OBLIVION_PATH/.obse.zip" ] && [ -w "$OBLIVION_PATH/.obse.zip" ]; then
        rm -f "$OBLIVION_PATH/.obse.zip"
    fi
    if [ -e "$OBLIVION_PATH/.northernui.zip" ] && [ -w "$OBLIVION_PATH/.northernui.zip" ]; then
        rm -f "$OBLIVION_PATH/.northernui.zip"
    fi
    if [ ! -e "$OBLIVION_PATH/OblivionLauncher.exe" ] && [ -e "$OBLIVION_PATH/OblivionLauncher.exe.osdi.bak" ]; then
        mv -f "$OBLIVION_PATH/OblivionLauncher.exe.osdi.bak" "$OBLIVION_PATH/OblivionLauncher.exe"
    fi
    exit $1
}

function isOblivionInstalledInPath ()
{
    TESTPATH="$1"
    if [ -d  "$TESTPATH/common/Oblivion" ] && [ -e "$TESTPATH/common/Oblivion/Oblivion.exe" ] && [ -e "$TESTPATH/common/Oblivion/OblivionLauncher.exe" ]; then
        return 0
    fi
    return 1
}

function locateOblivion ()
{
    if isOblivionInstalledInPath ~/.steam/root/steamapps/; then
        echo ~/.steam/root/steamapps/
        return
    fi
    if [ -e ~/.steam/root/config/libraryfolders.vdf ]; then
        while IFS= read -r otherDirectory; do
            if isOblivionInstalledInPath "$otherDirectory/steamapps/"; then
                echo "$otherDirectory/steamapps/"
                return
            fi
        done <<< "$(\grep path ~/.steam/root/config/libraryfolders.vdf|perl -p -E 's/\r//g;s/^\s+//;s/^\S+\s+//;s/^\"//;s/\"$//')"
    fi
}

function die ()
{
    EXITVAL=1
    MSG="$1"
    if [ "$2" != "" ]; then
        EXITVAL="$1"
        MSG="$2"
    fi
    echo 1>&2 "$MSG"
    echo 1>&2 "Unable to continue."
    cleanExit $EXITVAL
}

function downloadFileTo ()
{
    URL="$1"
    CHECKSUM="$2"
    DOWNLOAD_PATH="$3"
    if [ -e "$DOWNLOAD_PATH" ]; then
        rm -f "$DOWNLOAD_PATH"
    fi

    echo "Downloading $URL:"
    wget -q --show-progress -O "$DOWNLOAD_PATH" "$URL" || die "wget of $URL failed"

    if [ ! -e "$DOWNLOAD_PATH" ]; then
        die 6 "Download failed: file does not exist after finishing."
    fi

    if ! sha512sum "$DOWNLOAD_PATH" | grep -qE "^$CHECKSUM"; then
        die 5 "Failed to validate checksum for $DOWNLOAD_PATH"
    fi
}

function fixupTimestamps ()
{
    find "$OBLIVION_PATH/OBSE" -type f -iname '*dll' -print0 | xargs -0 touch
}

function UNZIP ()
{
    ORIGINAL_WD="$PWD"
    SOURCE_FILE="$1"
    TARGET_DIR="$2"

    # Reset env to avoid unzip doing stuff we don't want it to
    export UNZIP=""
    export UNZIPOPT=""

    cd "$TARGET_DIR" || die "Failed to switch to target directory: $TARGET_DIR"

    # -D:  Skip restoring timestamps. This is needed because Oblivion always
    #      loads the newest files, and when Steam installs Oblivion those files
    #      will have a timestamp matching the install time, so the mod files
    #      need to have a timestamp newer than that.
    # -qq: Makes unzip silent
    # -o:  Makes unzip overwrite files by default
    unzip -D -qq -o "$SOURCE_FILE" || die "Failed to unzip $SOURCE_FILE"

    cd "$ORIGINAL_WD" || cd "/"
}

function main ()
{
    echo "Oblivion SteamDeck mod installer version $VERSION"
    verifyDeps
    OBLIVION_PATH="$(locateOblivion)"
    if [ "$OBLIVION_PATH" == "" ] || [ ! -d "$OBLIVION_PATH" ] || ! isOblivionInstalledInPath "$OBLIVION_PATH"; then
        die 4 "Failed to locate Oblivion. Is it installed?"
    fi
    OBLIVION_PATH="$OBLIVION_PATH/common/Oblivion/"

    if [ -e "$OBLIVION_PATH/.oblivion-steamdeck.version" ]; then
        echo ""
        echo "Oblivion SteamDeck mod is already installed."
        if [ "$1" != "-f" ]; then
            echo "Press Control+C to abort re-installation."
            echo -n "Continuing with re-install in";
            for no in 10 9 8 7 6 5 4 3 2 1; do
                echo -n " $no"
                sleep 1
            done
            echo ""
        fi
        echo "Proceeding with reinstall"
    fi

    # Restore original OblivionLauncher.exe if one is there
    if [ -e "$OBLIVION_PATH/OblivionLauncher.exe.osdi.bak" ]; then
        rm -f "$OBLIVION_PATH/OblivionLauncher.exe"
        mv "$OBLIVION_PATH/OblivionLauncher.exe.osdi.bak" "$OBLIVION_PATH/OblivionLauncher.exe"
    fi
    echo "Found Oblivion at $OBLIVION_PATH"

    downloadFileTo "$NORTHERNUI_FILE" "$NORTHERNUI_SHA512SUM" "$OBLIVION_PATH/.northernui.zip"
    downloadFileTo "$OBSE_FILE" "$OBSE_SHA512SUM" "$OBLIVION_PATH/.obse.zip"

    UNZIP "$OBLIVION_PATH/.obse.zip" "$OBLIVION_PATH"
    UNZIP "$OBLIVION_PATH/.northernui.zip" "$OBLIVION_PATH/Data"

    # Back up OblivionLauncher
    mv -f "$OBLIVION_PATH/OblivionLauncher.exe" "$OBLIVION_PATH/OblivionLauncher.exe.osdi.bak" || die "Failed to back up OblivionLauncher.exe"
    # Use the obse_loader instead of OblivionLauncher
    mv -f "$OBLIVION_PATH/obse_loader.exe" "$OBLIVION_PATH/OblivionLauncher.exe" || die "Failed to rename obse_loader.exe to OblivionLauncher.exe"

    echo -n "1" > "$OBLIVION_PATH/.oblivion-steamdeck.version"

    echo ""
    echo "* * *"
    echo "Successfully installed the Oblivion SteamDeck mod."
    echo "It consists of:"
    echo " x Oblivion Script Extender from https://github.com/llde/xOBSE/"
    echo "   and"
    echo " NorthernUI Away from https://www.nexusmods.com/oblivion/mods/48577"
    echo ""
    echo "NOTE: In Steam, select 'Gamepad with joystick trackpad' as your input method."
    echo ""
    echo "Some of the in-game settings aren't accessible with only gamepad controls."
    echo "Hold down the Steam button and use the right trackpad to navigate with the mouse"
    echo "on those screens (actual gameplay doesn't require this, but for instance setting"
    echo " the game resolution does)"
    echo "* * *"

    echo ""
    read -p "Press enter to exit."

    cleanExit 0
}

main "$@"
