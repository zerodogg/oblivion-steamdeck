# Oblivion SteamDeck

This is an installer script that installs and configures *The Elder Scrolls IV:
Oblivion* for play on a Steam Deck. It installs the [Oblivion Script
Extender](https://github.com/llde/xOBSE/) and the [NorthernUI
Away](https://www.nexusmods.com/oblivion/mods/48577) mod, which adds back
proper gamepad support which is lacking in the Steam version.

## Installation

### Mod installation
Copy and paste the following in a Terminal window on your Steam Deck (to get
one switch to desktop mode and start the app `Konsole`):

```shell
\wget -O- https://gitlab.com/zerodogg/oblivion-steamdeck/-/raw/main/Oblivion-SteamDeck-install.sh | bash
```

### Configuration

Then switch back to game mode, and set the input method for Oblivion to "Gamepad
with joystick trackpad".

Play the game.

## Accessing settings

There are some settings in the game (like resolution) that you can't seem to
access with the gamepad. To work around this hold down the Steam button and use
the right trackpad as a mouse to navigate (click down on the right trackpad for
'right click'). This is only required to access the settings, when playing the
game you won't need this.

## Credits

This is just an installer that makes it easy to install the mods in question. I
have not created the mods themselves (which does all of the actual work), all
credit goes to the [xOBSE](https://github.com/llde/xOBSE/) and
[NorthernUI](https://www.nexusmods.com/oblivion/mods/48577) developers.
